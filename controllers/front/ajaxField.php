<?php
/**
 * 2007-2022 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2022 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class CustomersrefAjaxFieldModuleFrontController extends FrontController
{
    public function displayAjaxSave()
    {
        $reference = Tools::getValue('reference');

        if ($reference == "") {
            die(json_encode(array(
                'success' => false,
                // 'text' => $this->l('Please fill the reference field.')
            )));
        }

        if (!$this->context->cart->id && isset($_COOKIE[$this->context->cookie->getName()])) {
            $this->context->cart->add();
            $this->context->cookie->id_cart = (int) $this->context->cart->id;
        }

        $id_cart = (int)$this->context->cart->id;

        if ($id_cart <= 0) {
            die(json_encode(array(
                'success' => false,
                // 'text' => $this->l('There is no cart with this id.')
            )));
        }

        $id_customersref = Db::getInstance()->getValue(
            "SELECT `id_customersref`
            FROM `" . _DB_PREFIX_ . "customersref`
            WHERE `id_cart` = '" . (int)$id_cart . "'"
        );

        if (!empty($id_customersref)) {
            Db::getInstance()->execute(
                "UPDATE `" . _DB_PREFIX_ . "customersref`
                SET `customersref` = '" . pSQL($reference) . "'
                WHERE `id_customersref` = '" . (int)$id_customersref . "'"
            );
        } else {
            Db::getInstance()->execute(
                "INSERT INTO `" . _DB_PREFIX_ . "customersref` (`id_cart`, `customersref`)
                VALUES ('" . (int)$id_cart . "', '" . pSQL($reference) . "')"
            );
        }

        die(json_encode(array('success' => true)));
    }

    public function displayAjaxDelete()
    {
        $id_cart = (int)$this->context->cart->id;

        if ($id_cart <= 0) {
            die(json_encode(array(
                'success' => false,
                // 'text' => $this->l('There is no cart with this id.')
            )));
        }

        $id_customersref = Db::getInstance()->getValue(
            "SELECT `id_customersref`
            FROM `" . _DB_PREFIX_ . "customersref`
            WHERE `id_cart` = '" . (int)$id_cart . "'"
        );

        Db::getInstance()->execute(
            "DELETE FROM `" . _DB_PREFIX_ . "customersref`
            WHERE `id_customersref` = '" . (int)$id_customersref . "'"
        );

        die(json_encode(array('success' => true)));
    }
}
