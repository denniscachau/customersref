{*
* 2007-2022 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2022 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="customersreference_div row">
    <div class="col-md-6">
        <div class="box">
            <form class="form-inline" id="cr-form" method="post">
                <div class="customersreference_step1 card-block"{if isset($cr_reference)} hidden{/if}>
                    <fieldset>
                        <div class="form-group">
                            <h4><label for="customersref">{l s='Your order reference (optional):' mod='customersref'}</label></h4>
                            <input class="form-control" id="customersref" name="customersref" type="text" style="width:219px;margin-right:11px;">
                            <button name="addRef" data-role="save" class="button btn btn-default button-small cr-btn" type="submit" value="">
                                <span>{l s='Save' mod='customersref'}</span>
                            </button>
                        </div>
                    </fieldset>
                </div>
                <div class="customersreference_step2"{if !isset($cr_reference)} hidden{/if}>
                    <h4><label>{l s='Your order reference:' mod='customersref'} <span id="cr-label">{if isset($cr_reference)}{$cr_reference|escape:'htmlall':'UTF-8'}{/if}</span></label></h4>
                    <button data-role="edit" class="button btn btn-default button-small cr-btn" type="button" value="">
                        <span>{l s='Modify' mod='customersref'}</span>
                    </button>
                    <button data-role="delete" class="button btn btn-default button-small cr-btn" type="submit" value="">
                        <span>{l s='Delete' mod='customersref'}</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", (e) => {
        function displayRecap() {
            let step1 = $('.customersreference_step1');
            let step2 = $('.customersreference_step2');

            $('.customersreference_step1').attr('hidden', '');
            $('.customersreference_step2').removeAttr('hidden');
        }

        function displayForm() {
            let step1 = $('.customersreference_step1');
            let step2 = $('.customersreference_step2');

            $('.customersreference_step1').removeAttr('hidden');
            $('.customersreference_step2').attr('hidden', '');
        }

        $('.cr-btn').on('click', function(e) {
            let cr_url = '{$cr_link->getModuleLink('customersref', 'ajaxField')|escape:'javascript':'UTF-8'}';
            switch ($(this).data('role')) {
                case 'save':
                    $.post(cr_url, {
                        action: 'save',
                        ajax: 1,
                        reference: $('#customersref').val()
                    }, (data) => {
                        data = JSON.parse(data);

                        if (data.success) {
                            $('#cr-label').text($('#customersref').val());
                            displayRecap();
                        } else if (typeof data.text !== 'undefined') {
                            console.log(data.text);
                        } else {
                            console.log("An error has occurred.");
                        }
                    });
                    break;
                case 'edit':
                    $('#customersref').val($('#cr-label').text());
                    displayForm();
                    break;
                case 'delete':
                    $.post(cr_url, {
                        action: 'delete',
                        ajax: 1
                    }, (data) => {
                        data = JSON.parse(data);

                        if (data.success) {
                            $('#customersref').val("");
                            displayForm();
                        } else if (typeof data.text !== 'undefined') {
                            console.log(data.text);
                        } else {
                            console.log("An error has occurred.");
                        }
                    });
                    break;
            }

            e.preventDefault();
        });
    });

</script>
