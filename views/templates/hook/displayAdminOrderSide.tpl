{*
* 2007-2022 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2022 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md">
                <h3 class="card-header-title">{l s='Customer\'s reference' mod='customersref'}</h3>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form method="post">
            <input type="hidden" name="CR_orderleft" value="1">
            {if isset($text)}
                <div id="customerRefValidate">
                    <h4>{$text|escape:'htmlall':'UTF-8'}</h4>
                    <div class="text-right">
                        <button class="btn btn-primary" type="button" onclick="hideForm()">{l s='Modify' mod='customersref'}</button>
                        <button name="CR_delete" value="1" class="btn btn-danger" type="submit">{l s='Delete' mod='customersref'}</button>
                    </div>
                </div>
            {/if}

            <div id="customerRefForm" {if !empty($cr_reference)}hidden{/if}>
                <div class="form-group">
                    <div class="row">
                        <label class="control-label col-12" for="customersref">{l s='His reference: ' mod='customersref'} </label>
                        <div class="col-12">
                            <div class="input-group">
                                <input class="form-control" id="customersref" name="customersref" type="text" value="{$cr_reference|escape:'htmlall':'UTF-8'}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-right">
                    <input name="CR_modify" class="btn btn-primary" type="submit" value="{l s='Validate' mod='customersref'}">
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    function hideForm() {
        document.getElementById('customerRefValidate').setAttribute('hidden', '');
        document.getElementById('customerRefForm').removeAttribute('hidden');
    }
</script>
