{*
* 2007-2022 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2022 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div style="padding:10px; margin:15px 0">
  <div class="panel">
    <form method="post">
      <input type="hidden" name="CR_orderleft" value="1">
      {if isset($text)}
        <div id="customerRefValidate">
          <span style="font-size:1.2em"><strong>{$text|escape:'htmlall':'UTF-8'}</strong></span>
          <button class="btn btn-default" type="button" onclick="hideForm()" style="margin-right:15px">{l s='Modify' mod='customersref'}</button>
          <button name="CR_delete" value="1" class="btn btn-default" type="submit">{l s='Delete' mod='customersref'}</button>
        </div>
      {/if}

      <div id="customerRefForm" class="form-group" {if !empty($cr_reference)}hidden{/if}>
        <label class="control-label col-lg-3" for="customersref" style="padding-top:5px">{l s='His reference: ' mod='customersref'} </label>
        <input class="col-lg-6" id="customersref" name="customersref" type="text" value="{$cr_reference|escape:'htmlall':'UTF-8'}" style="margin-right:15px;width:30%">
        <input name="CR_modify" class="col-lg btn btn-default" type="submit" value="{l s='Validate' mod='customersref'}">
      </div>
    </form>
  </div>
</div>

<script>
  function hideForm() {
    document.getElementById('customerRefValidate').setAttribute('hidden', '');
    document.getElementById('customerRefForm').removeAttribute('hidden');
  }
</script>
