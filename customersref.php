<?php
/**
* 2007-2022 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2022 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Customersref extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'customersref';
        $this->tab = 'front_office_features';
        $this->version = '1.5.0';
        $this->author = 'Ciren';
        $this->need_instance = 1;
        $this->module_key = '0ae2c743e7264e72e383b503ad67e8dd';

        /*
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Add customer reference for accounting');
        $this->description =
            $this->l('This module allows your customers to add their own reference on their order to facilitate better bill management.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '8.0.99');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (!parent::install()) {
            return false;
        }

        include dirname(__FILE__) . '/sql/install.php';

        if (version_compare(_PS_VERSION_, '1.7.7.0', '>=')) {
            $this->registerHook('actionOrderGridQueryBuilderModifier');
            $this->registerHook('actionOrderGridDefinitionModifier');
            $this->registerHook('displayAdminOrderSide');
        } else {
            $this->registerHook('actionAdminOrdersListingFieldsModifier');
            if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
                $this->registerHook('displayAdminOrder');
            } else {
                $this->registerHook('displayAdminOrderLeft');
            }
        }

        return $this->registerHook('displayShoppingCart') &&
            $this->registerHook('displayShoppingCartFooter') &&
            $this->registerHook('displayCustomersReference') &&
            $this->registerHook('displayOrderDetail') &&
            $this->registerHook('displayPDFInvoice') &&
            $this->registerHook('displayPDFDeliverySlip') &&
            // $this->registerHook('displayHeader') &&
            $this->registerHook('actionOrderStatusPostUpdate') &&
            $this->registerHook('actionValidateOrder') &&
            $this->registerHook('sendMailAlterTemplateVars');
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        include dirname(__FILE__) . '/sql/uninstall.php';

        return true;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /*
         * If values have been submitted in the form, process.
         */
        if (((bool) Tools::isSubmit('submitCustomersrefModule')) == true) {
            $this->postProcess();
        }

        return $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCustomersrefModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    protected function getConfigFormValues()
    {
        $configs = Db::getInstance()->getRow(
            "SELECT `active`, `id_hook`
            FROM `" . _DB_PREFIX_ . $this->name . "_configuration`"
        );

        $return = array();

        if (!empty($configs)) {
            $return['active'] = $configs['active'];
            $return['id_hook'] = $configs['id_hook'];
        } else {
            $return['active'] = false;
            $return['id_hook'] = Hook::getIdByName('displayShoppingCart');
        }

        $configs_lang = Db::getInstance()->executeS(
            "SELECT `id_lang`, `label_front`, `label_invoice`
            FROM `" . _DB_PREFIX_ . $this->name . "_configuration_lang`"
        );

        foreach (Language::getLanguages() as $lang) {
            $return['label_front'][$lang['id_lang']] = '';
            $return['label_invoice'][$lang['id_lang']] = '';
        }
        foreach ($configs_lang as $config) {
            $return['label_front'][$config['id_lang']] = $config['label_front'];
            $return['label_invoice'][$config['id_lang']] = $config['label_invoice'];
        }

        return $return;
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $id_shopping_cart = Hook::getIdByName('displayShoppingCart');
        $id_shopping_cart_footer = Hook::getIdByName('displayShoppingCartFooter');
        $id_customersref = Hook::getIdByName('displayCustomersReference');
        $hooks = array(
            array(
                'id' => $id_shopping_cart_footer,
                'name' => $this->l('Below the list of products') . " (displayShoppingCartFooter)",
            ),
            array(
                'id' => $id_shopping_cart,
                'name' => $this->l('Above the order summary') . " (displayShoppingCart)",
            ),
            array(
                'id' => $id_customersref,
                'name' => $this->l('Custom hook') . " (displayCustomersReference)",
            ),
        );

        $this->context->controller->informations[] =
            $this->l('The custom hook doesn\'t exist by default.') . "<br>" .
            $this->l('If you want to use it, you need to add:') .
            " {hook h='displayCustomersReference'} " .
            $this->l('at the position where you want to display the text field (see documentation for more details)');

        $this->context->controller->informations[] = "<br>";

        $this->context->controller->informations[] =
            $this->l('You can modify the display of the text field to adapt it to your theme.') . " " .
            $this->l('You have all classes to modify written in documentation.');

        $this->context->controller->informations[] = "<br>";

        $this->context->controller->informations[] =
            $this->l('You can translate all the text of the module by clicking on the "Translate" button at the top right of the screen.');

        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'name' => 'active',
                        'label' => $this->l('Active the display on "Order Setting": '),
                        'desc' => $this->l('Activate or desactivate the extra column in "Order Settings (admin) '),
                        'values' => array(
                            array(
                                'id' => 'active',
                                'value' => true,
                                'label' => $this->l('Yes'),
                            ),
                            array(
                                'id' => 'inactive',
                                'value' => false,
                                'label' => $this->l('No'),
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'id_hook',
                        'label' => $this->l('Where do you want to display the field for customers (In the cart page)'),
                        'options' => array(
                            'query' => $hooks,
                            'id' => 'id',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Text block you want to display on the order details of the customer : '),
                        'name' => 'label_front',
                        'class' => 'textarea-autosize label-front',
                        'cols' => 40,
                        'rows' => 10,
                        'lang' => true,
                        'desc' =>
                            $this->l('The custom reference has to be surrounded by 2') .
                            $this->l('percentages like that : "%%reference%%".'),
                        'hint' =>
                            $this->l('If you have several languages installed on your shop, don\'t forget to fill ') .
                            $this->l('the field in all the languages otherwise it will be displayed a default text ') .
                            $this->l('("Your order reference is : %%reference%%").'),
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Text block you want to display on the invoice : '),
                        'name' => 'label_invoice',
                        'class' => 'textarea-autosize label-invoice',
                        'cols' => 40,
                        'rows' => 10,
                        'lang' => true,
                        'desc' => $this->l('The custom reference has to be surrounded by 2') .
                            $this->l('percentages like that : "%%reference%%".'),
                        'hint' =>
                            $this->l('If you have several languages installed on your shop, don\'t forget to fill ') .
                            $this->l('the field in all the languages otherwise it will be displayed a default text ') .
                            $this->l('("Your order reference is : %%reference%%").'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $active = Tools::getValue('active') ? 1 : 0;
        $id_hook = Tools::getValue('id_hook');
        $sql = array();

        foreach (Language::getLanguages() as $language) {
            $label_front = Tools::getValue('label_front_' . $language['id_lang']);
            $preg_front = preg_match('/%%reference%%/', $label_front);
            $label_invoice = Tools::getValue('label_invoice_' . $language['id_lang']);
            $preg_invoice = preg_match('/%%reference%%/', $label_invoice);

            if (!$preg_invoice && $label_invoice != '') {
                $this->context->controller->errors[] =
                    $this->l('The variable %%reference%% must be specified in the text for the invoice.');

                $tmp = Db::getInstance()->getValue(
                    "SELECT `label_invoice`
                    FROM `" . _DB_PREFIX_ . $this->name . "_configuration_lang`
                    WHERE `id_lang` = '" . (int)$language['id_lang'] . "'"
                );

                $label_invoice = !empty($tmp) ? $tmp : "";
            }
            if (!$preg_front && $label_front != '') {
                $this->context->controller->errors[] =
                    $this->l('The variable %%reference%% must be specified in the text for the order details.');

                $tmp = Db::getInstance()->getValue(
                    "SELECT `label_front`
                    FROM `" . _DB_PREFIX_ . $this->name . "_configuration_lang`
                    WHERE `id_lang` = '" . (int)$language['id_lang'] . "'"
                );

                $label_front = !empty($tmp) ? $tmp : "";
            }

            $sql[] = "INSERT INTO `" . _DB_PREFIX_ . $this->name . "_configuration_lang`
                (`id_lang`, `label_front`, `label_invoice`)
                VALUES ('" . (int)$language['id_lang'] . "',
                '" . pSQL($label_front) . "',
                '" . pSQL($label_invoice) . "')";
        }

        Db::getInstance()->execute("DELETE FROM `" . _DB_PREFIX_ . $this->name . "_configuration_lang`");
        Db::getInstance()->execute("DELETE FROM `" . _DB_PREFIX_ . $this->name . "_configuration`");

        foreach ($sql as $query) {
            Db::getInstance()->execute($query);
        }

        Db::getInstance()->execute(
            "INSERT INTO `" . _DB_PREFIX_ . $this->name . "_configuration` (`active`, `id_hook`)
            VALUES ('" . (int)$active . "', '" . (int)$id_hook . "')"
        );

        return true;
    }

    // dans le panier en haut a droite
    public function hookDisplayShoppingCart()
    {
        $configuration = Db::getInstance()->getValue(
            "SELECT `id_hook`
            FROM `" . _DB_PREFIX_ . $this->name . "_configuration`"
        );

        $id_shopping_cart = Hook::getIdByName('displayShoppingCart');
        if ($configuration == $id_shopping_cart) {
            $this->renderFrontForm();

            if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
                return $this->display(__FILE__, 'displayShoppingCart16.tpl');
            } else {
                return $this->display(__FILE__, 'displayShoppingCartAjax.tpl');
            }
        }
    }

    // dans le panier dessous
    public function hookDisplayShoppingCartFooter()
    {
        $configuration = Db::getInstance()->getValue(
            "SELECT `id_hook`
            FROM `" . _DB_PREFIX_ . $this->name . "_configuration`"
        );

        $id_shopping_cart_footer = Hook::getIdByName('displayShoppingCartFooter');
        if ($configuration == $id_shopping_cart_footer) {
            $this->renderFrontForm();

            if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
                return $this->display(__FILE__, 'displayShoppingCartFooter16.tpl');
            } else {
                return $this->display(__FILE__, 'displayShoppingCartFooterAjax.tpl');
            }
        }
    }

    // dans le panier dessous
    public function hookDisplayCustomersReference()
    {
        $configuration = Db::getInstance()->getValue(
            "SELECT `id_hook`
            FROM `" . _DB_PREFIX_ . $this->name . "_configuration`"
        );

        $id_customersref_hook = Hook::getIdByName('displayCustomersReference');
        if ($configuration == $id_customersref_hook) {
            if (!$this->context->cart->id && isset($_COOKIE[$this->context->cookie->getName()])) {
                $this->context->cart->add();
                $this->context->cookie->id_cart = (int) $this->context->cart->id;
            }

            if (!empty($this->context->cart->id)) {
                $this->renderFrontForm();

                if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
                    return $this->display(__FILE__, 'displayCustomersReference16.tpl');
                } else {
                    return $this->display(__FILE__, 'displayCustomersReferenceAjax.tpl');
                }
            }
        }
    }

    private function renderFrontForm()
    {
        $id_customersref = Db::getInstance()->getValue(
            "SELECT `id_customersref`
            FROM `" . _DB_PREFIX_ . $this->name . "`
            WHERE `id_cart` = '" . (int)$this->context->cart->id . "'"
        );

        if (Tools::getValue('delRef')) {
            Db::getInstance()->execute(
                "DELETE FROM `" . _DB_PREFIX_ . $this->name . "`
                WHERE `id_customersref` = '" . (int)$id_customersref . "'"
            );
        } elseif (Tools::getValue('addRef') && $reference = Tools::getValue('customersref')) {
            if (!empty($id_customersref)) {
                Db::getInstance()->execute(
                    "UPDATE `" . _DB_PREFIX_ . $this->name . "`
                    SET `customersref` = '" . pSQL($reference) . "'
                    WHERE `id_customersref` = '" . (int)$id_customersref . "'"
                );
            } else {
                Db::getInstance()->execute(
                    "INSERT INTO `" . _DB_PREFIX_ . $this->name . "` (`id_cart`, `customersref`)
                    VALUES ('" . (int)$this->context->cart->id . "', '" . pSQL($reference) . "')"
                );
            }
        }

        if (!isset($reference) && !Tools::getValue('delRef')) {
            $reference = Db::getInstance()->getValue(
                "SELECT `customersref`
                FROM `" . _DB_PREFIX_ . $this->name . "`
                WHERE `id_cart` = '" . (int)$this->context->cart->id . "'"
            );
        }

        if (isset($reference) && $reference) {
            $this->context->smarty->assign(array(
                'cr_reference' => $reference,
            ));
        }

        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $this->context->smarty->assign(array(
                'cr_link' => $this->context->link,
            ));
        }
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        $this->updateOrderId($params['id_order']);
    }

    public function hookActionValidateOrder($params)
    {
        $this->updateOrderId($params['order']->id);
    }

    public function updateOrderId($id_order)
    {
        if ($id_cart = Db::getInstance()->getValue(
            "SELECT `id_cart`
            FROM `" . _DB_PREFIX_ . "orders`
            WHERE `id_order` = '" . (int)$id_order . "'"
        )) {
            Db::getInstance()->execute(
                "UPDATE `" . _DB_PREFIX_ . $this->name . "`
                SET `id_order` = '" . (int)$id_order . "'
                WHERE `id_cart` = '" . (int)$id_cart . "'"
            );
        }
    }

    // Details d'une commande (front)
    public function hookDisplayOrderDetail($params)
    {
        if ($this->displayReference('label_front', $params['order']->id)) {
            return $this->context->smarty->fetch($this->local_path . 'views/templates/hook/displayOrderDetail.tpl');
        }
    }

    // Sur la facture en bas
    public function hookDisplayPDFInvoice($params)
    {
        if ($this->displayReference('label_invoice', $params['object']->id_order)) {
            return $this->context->smarty->fetch($this->local_path . 'views/templates/hook/displayPDFInvoice.tpl');
        }
    }

    // Sur la facture en bas
    public function hookDisplayPDFDeliverySlip($params)
    {
        if ($this->displayReference('label_invoice', $params['object']->id_order)) {
            return $this->context->smarty->fetch($this->local_path . 'views/templates/hook/displayPDFDeliverySlip.tpl');
        }
    }

    public function hookDisplayAdminOrderLeft()
    {
        return $this->displayAdminOrder(Tools::getValue('id_order'), 'displayAdminLeftOrder');
    }

    public function hookDisplayAdminOrder()
    {
        return $this->displayAdminOrder(Tools::getValue('id_order'), 'displayAdminLeftOrder');
    }

    public function hookDisplayAdminOrderSide($params)
    {
        return $this->displayAdminOrder($params['id_order'], 'displayAdminOrderSide');
    }

    private function displayAdminOrder($id_order, $template)
    {
        if (Tools::getIsset('CR_orderleft')) {
            if (Tools::getIsset('CR_modify')) {
                if (Db::getInstance()->getValue(
                    "SELECT `customersref` FROM `" . _DB_PREFIX_ . "customersref`
                    WHERE `id_order` = '" . (int) $id_order . "'"
                )) {
                    Db::getInstance()->execute(
                        "UPDATE `" . _DB_PREFIX_ . "customersref`
                        SET `customersref` = '" . pSQL(Tools::getValue('customersref')) . "'
                        WHERE `id_order` = '" . (int) $id_order . "'"
                    );
                } else {
                    $id_cart = Db::getInstance()->getValue(
                        "SELECT `id_cart` FROM `" . _DB_PREFIX_ . "orders`
                        WHERE `id_order` = '" . (int) $id_order . "'"
                    );
                    Db::getInstance()->execute(
                        "INSERT INTO `" . _DB_PREFIX_ . "customersref` (`customersref`, `id_cart`, `id_order`)
                        VALUES ('" . pSQL(Tools::getValue('customersref')) . "',
                        '" . (int) $id_cart . "',
                        '" . (int) $id_order . "')"
                    );
                }
            }
            if (Tools::getIsset('CR_delete')) {
                Db::getInstance()->execute(
                    "DELETE FROM `" . _DB_PREFIX_ . "customersref`
                    WHERE `id_order` = '" . (int) $id_order . "'"
                );
            }
        }
        $this->displayReference('label_back', $id_order);
        $this->context->smarty->assign(array(
            'cr_reference' => Db::getInstance()->getValue(
                "SELECT `customersref` FROM `" . _DB_PREFIX_ . "customersref`
                WHERE `id_order` = '" . (int)$id_order . "'"
            )
        ));
        return $this->context->smarty->fetch($this->local_path . 'views/templates/hook/' . $template . '.tpl');
    }

    private function displayReference($office, $id_order)
    {
        $reference = Db::getInstance()->getValue(
            "SELECT `customersref`
            FROM `" . _DB_PREFIX_ . $this->name . "`
            WHERE `id_order` = '" . (int)$id_order . "'"
        );
        if (empty($reference)) {
            return false;
        }

        if ($office != 'label_back') {
            $label = Db::getInstance()->getValue(
                "SELECT `" . $office . "`
                FROM `" . _DB_PREFIX_ . $this->name . "_configuration_lang`
                WHERE `id_lang` = '" . (int)$this->context->language->id . "'"
            );
        }

        if (empty($label) && ($office == 'label_front' || $office == 'label_invoice')) {
            $label = $this->l('Your order reference is : %%reference%%');
        } elseif ($office == 'label_back') {
            $label = $this->l('His order reference is : %%reference%%');
        }

        preg_match_all("/%%(\w*)%%/", $label, $var_bruts);
        $text = preg_split('/%%\w*%%/', $label); // Removal of the variable in the text
        $buffer_text = '';
        for ($key=0; $key < sizeof($var_bruts[1]); $key++) {
            $buffer_text .= $text[$key] . $reference;
        }

        if (array_key_exists($key, $text)) {
            $text = $buffer_text . $text[$key];
        } else {
            $text = $buffer_text;
        }

        $this->context->smarty->assign(array(
            'text' => $text
        ));
        return true;
    }

    public function hookActionAdminOrdersListingFieldsModifier($params)
    {
        if (Db::getInstance()->getValue('SELECT `active` FROM `'._DB_PREFIX_.$this->name.'_configuration`')) {
            if (isset($params['select'])) {
                $params['select'] .= ", `cr`.`customersref`";
            } else {
                $params['select'] = "`cr`.`customersref`";
            }

            if (isset($params['join'])) {
                $params['join'] .= " LEFT JOIN `" . _DB_PREFIX_ . $this->name . "` `cr`
                    ON (`cr`.`id_order` = `a`.`id_order`)";
            } else {
                $params['join'] = " LEFT JOIN `" . _DB_PREFIX_ . $this->name . "` `cr`
                    ON (`cr`.`id_order` = `a`.`id_order`)";
            }

            $params['fields']['customersref'] = array(
                'title' => $this->l('Customer reference'),
            );
        }
    }

    public function hookActionOrderGridQueryBuilderModifier($params)
    {
        if (Db::getInstance()->getValue('SELECT `active` FROM `'._DB_PREFIX_.$this->name.'_configuration`')) {
            $params['search_query_builder']->addSelect("`cr`.`customersref` AS 'cr-customersref'");

            $params['search_query_builder']->leftJoin(
                'o',
                _DB_PREFIX_ . $this->name,
                'cr',
                '`o`.`id_order` = `cr`.`id_order`'
            );

            $filters = $params['search_criteria']->getFilters();

            if (!empty($filters) && array_key_exists('cr-customersref', $filters)) {
                $params['search_query_builder']->andWhere(
                    "`cr`.`customersref` LIKE '%" . $filters['cr-customersref'] . "%'"
                );
            }
        }
    }

    public function hookActionOrderGridDefinitionModifier($params)
    {
        if (Db::getInstance()->getValue('SELECT `active` FROM `'._DB_PREFIX_.$this->name.'_configuration`')) {
            $columns = $params['definition']->getColumns();
            $filters = $params['definition']->getFilters();

            $columns->addBefore(
                'actions',
                (new PrestaShop\PrestaShop\Core\Grid\Column\Type\DataColumn('cr-customersref'))
                    ->setName($this->l('Customer reference'))
                    ->setOptions([
                        'field' => 'cr-customersref',
                    ])
            );

            $filters->remove('new');
            $filters->add(
                (new PrestaShop\PrestaShop\Core\Grid\Filter\Filter(
                    'cr-customersref',
                    Symfony\Component\Form\Extension\Core\Type\TextType::class
                ))->setTypeOptions(
                    array(
                        'required' => false,
                        'attr' => array(
                            'placeholder' => $this->l('Customer reference'),
                        ),
                    )
                )->setAssociatedColumn('cr-customersref')
            );
        }
    }

    public function hookSendMailAlterTemplateVars($params) {
        $templateVars = $params['template_vars'];

        if (isset($templateVars['{id_order}'])) {
            $id_order = (int)$templateVars['{id_order}'];

            $reference = Db::getInstance()->getValue(
                "SELECT `customersref`
                FROM `" . _DB_PREFIX_ . $this->name . "`
                WHERE `id_order` = '" . (int)$id_order . "'"
            );

            $params['template_vars']['{customersref}'] = $reference;
        }
    }
}
