<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{customersref}prestashop>customersref_18c2bcb1dec1edef81af9817047b89d6'] = 'Aggiungi riferimento cliente per la contabilità';
$_MODULE['<{customersref}prestashop>customersref_f861f861ef48fb0d7927db59c873c021'] = 'Questo modulo consente ai tuoi clienti di aggiungere i loro riferimenti sul loro ordine per facilitare una migliore gestione delle fatture.';
$_MODULE['<{customersref}prestashop>customersref_89406051d1d5a69c9b5fdbb2cc10d454'] = 'Sei sicuro di voler disinstallare questo modulo?';
$_MODULE['<{customersref}prestashop>customersref_dc4f37ffcb78e214418fbbb38b1aba51'] = 'Sotto l\'elenco dei prodotti';
$_MODULE['<{customersref}prestashop>customersref_ad8c49d4c8b73c53b213662e266399e5'] = 'Sopra il riepilogo dell\'ordine';
$_MODULE['<{customersref}prestashop>customersref_678314871893087a13885397b9359d70'] = 'Gancio personalizzato';
$_MODULE['<{customersref}prestashop>customersref_74051c016db47f0208f42a608881c8de'] = 'L\'hook personalizzato non esiste per impostazione predefinita.';
$_MODULE['<{customersref}prestashop>customersref_2f5386cc8c6c0bad14415ce111edbd6e'] = 'Se vuoi usarlo, devi aggiungere:';
$_MODULE['<{customersref}prestashop>customersref_2ee0b15c1bfb1c943ab0aae22d75165f'] = 'nella posizione in cui si desidera visualizzare il campo di testo (consultare la documentazione per maggiori dettagli)';
$_MODULE['<{customersref}prestashop>customersref_9e85f55bfa32d9badaf0818d697e4af0'] = 'È possibile modificare la visualizzazione del campo di testo per adattarlo al tema.';
$_MODULE['<{customersref}prestashop>customersref_d141cec94f9854b3ec1cceec3611b416'] = 'Hai tutte le classi da modificare scritte nella documentazione.';
$_MODULE['<{customersref}prestashop>customersref_d4ab2f0345485890ab9d1ba0814de340'] = 'Puoi tradurre tutto il testo del modulo facendo clic sul pulsante \"Traduci\" nella parte superiore destra dello schermo.';
$_MODULE['<{customersref}prestashop>customersref_f4f70727dc34561dfde1a3c529b6205c'] = 'Impostazioni';
$_MODULE['<{customersref}prestashop>customersref_ae62a3767e04645f09ab7918ec1caf1c'] = 'Attiva la visualizzazione su \"Impostazione ordine\":';
$_MODULE['<{customersref}prestashop>customersref_d31b131ec4011e1393ef182155cb0181'] = 'Attiva o disattiva la colonna aggiuntiva in \"Impostazioni ordine\" (admin)';
$_MODULE['<{customersref}prestashop>customersref_93cba07454f06a4a960172bbd6e2a435'] = 'Sì';
$_MODULE['<{customersref}prestashop>customersref_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{customersref}prestashop>customersref_c5b2556898f3b4477dc934bebd485cf4'] = 'Dove vuoi visualizzare il campo per i clienti (nella pagina del carrello)';
$_MODULE['<{customersref}prestashop>customersref_65fe7534bb9003bf1abd20b19b36b6ce'] = 'Blocco di testo che desideri visualizzare sui dettagli dell\'ordine del cliente:';
$_MODULE['<{customersref}prestashop>customersref_e4afac7db515d16c6e27bf554ce2de18'] = 'Il riferimento personalizzato deve essere racchiuso tra 2 ';
$_MODULE['<{customersref}prestashop>customersref_6c01aecc7df3082d6b9ebb63ca966f5e'] = 'percentuali del genere: \"%%reference%%\".';
$_MODULE['<{customersref}prestashop>customersref_c9b33cb4177026628a3646352d2910f2'] = 'Se hai diverse lingue installate nel tuo negozio, non dimenticare di riempire il ';
$_MODULE['<{customersref}prestashop>customersref_2502ce310864affe51a634740dec0c50'] = 'in tutte le lingue, altrimenti verrà visualizzato un testo predefinito ';
$_MODULE['<{customersref}prestashop>customersref_8d23e861b6cc7bd9493aace169388081'] = '( \"Il riferimento dell\'ordine è: %%reference%%\").';
$_MODULE['<{customersref}prestashop>customersref_b384634655235f5b98108ae7d2e95a28'] = 'Blocco di testo che desideri visualizzare sulla fattura:';
$_MODULE['<{customersref}prestashop>customersref_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{customersref}prestashop>customersref_1c5fd7c921132c17266d33a10f3022cd'] = 'La variabile %%reference%% deve essere specificata nel testo per la fattura.';
$_MODULE['<{customersref}prestashop>customersref_2b2ac905e3c4c809fc3227d824b42ed2'] = 'La variabile %%reference%% deve essere specificata nel testo per i dettagli dell\'ordine.';
$_MODULE['<{customersref}prestashop>customersref_800ef6d59ae55cf6b0095e924b4fd3c2'] = 'Il riferimento dell\'ordine è: %%reference%%';
$_MODULE['<{customersref}prestashop>customersref_6fae8a25c8b49b5cb4ad2159a2d409ef'] = 'Il suo riferimento all\'ordine è: %%reference%%';
$_MODULE['<{customersref}prestashop>customersref_3038bdd47781aed5d47d88058ed38e1d'] = 'Riferimento cliente';
$_MODULE['<{customersref}prestashop>displayadminorderside_116eee83c39d2eb1dd1f4fbe181e0524'] = 'Riferimento del cliente';
$_MODULE['<{customersref}prestashop>displayadminorderside_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displayadminorderside_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displayadminorderside_2b639a6072f5720fc25603927196e8ef'] = 'Il suo riferimento:';
$_MODULE['<{customersref}prestashop>displayadminorderside_ad3d06d03d94223fa652babc913de686'] = 'Convalidare';
$_MODULE['<{customersref}prestashop>displayadminleftorder_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displayadminleftorder_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displayadminleftorder_2b639a6072f5720fc25603927196e8ef'] = 'Il suo riferimento:';
$_MODULE['<{customersref}prestashop>displayadminleftorder_ad3d06d03d94223fa652babc913de686'] = 'Convalidare';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooterajax_5ce96cc749b444487611a74026a163bb'] = 'Riferimento dell\'ordine (opzionale):';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooterajax_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooterajax_fe8cc190e37aa5083375088bd5873c29'] = 'Riferimento dell\'ordine:';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooterajax_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooterajax_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooter_72b2338b31650f36d0ef194794885332'] = 'Il tuo riferimento:';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooter_ad3d06d03d94223fa652babc913de686'] = 'Convalidare';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooter_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooter_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooter16_5ce96cc749b444487611a74026a163bb'] = 'Riferimento dell\'ordine (opzionale):';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooter16_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooter16_fe8cc190e37aa5083375088bd5873c29'] = 'Riferimento dell\'ordine:';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooter16_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displayshoppingcartfooter16_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displaycustomersreference16_5ce96cc749b444487611a74026a163bb'] = 'Riferimento dell\'ordine (opzionale):';
$_MODULE['<{customersref}prestashop>displaycustomersreference16_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{customersref}prestashop>displaycustomersreference16_fe8cc190e37aa5083375088bd5873c29'] = 'Riferimento dell\'ordine:';
$_MODULE['<{customersref}prestashop>displaycustomersreference16_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displaycustomersreference16_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displaycustomersreference_72b2338b31650f36d0ef194794885332'] = 'Il tuo riferimento:';
$_MODULE['<{customersref}prestashop>displaycustomersreference_ad3d06d03d94223fa652babc913de686'] = 'Convalidare';
$_MODULE['<{customersref}prestashop>displaycustomersreference_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displaycustomersreference_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displayshoppingcart16_5ce96cc749b444487611a74026a163bb'] = 'Riferimento dell\'ordine (opzionale):';
$_MODULE['<{customersref}prestashop>displayshoppingcart16_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{customersref}prestashop>displayshoppingcart16_fe8cc190e37aa5083375088bd5873c29'] = 'Riferimento dell\'ordine:';
$_MODULE['<{customersref}prestashop>displayshoppingcart16_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displayshoppingcart16_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displayshoppingcart_72b2338b31650f36d0ef194794885332'] = 'Il tuo riferimento:';
$_MODULE['<{customersref}prestashop>displayshoppingcart_ad3d06d03d94223fa652babc913de686'] = 'Convalidare';
$_MODULE['<{customersref}prestashop>displayshoppingcart_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displayshoppingcart_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displayshoppingcartajax_5ce96cc749b444487611a74026a163bb'] = 'Riferimento dell\'ordine (opzionale):';
$_MODULE['<{customersref}prestashop>displayshoppingcartajax_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{customersref}prestashop>displayshoppingcartajax_fe8cc190e37aa5083375088bd5873c29'] = 'Riferimento dell\'ordine:';
$_MODULE['<{customersref}prestashop>displayshoppingcartajax_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displayshoppingcartajax_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{customersref}prestashop>displaycustomersreferenceajax_5ce96cc749b444487611a74026a163bb'] = 'Riferimento dell\'ordine (opzionale):';
$_MODULE['<{customersref}prestashop>displaycustomersreferenceajax_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{customersref}prestashop>displaycustomersreferenceajax_fe8cc190e37aa5083375088bd5873c29'] = 'Riferimento dell\'ordine:';
$_MODULE['<{customersref}prestashop>displaycustomersreferenceajax_7f090bbab1cc7f9c08bf4e54d932d3c0'] = 'Modificare';
$_MODULE['<{customersref}prestashop>displaycustomersreferenceajax_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
